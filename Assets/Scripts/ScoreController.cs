﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class ScoreController : MonoBehaviour
{
    [SerializeField]
    string _savePath = "Saves";
    [SerializeField]
    string _saveName = "save.bin";

    private string _filePath;

    [SerializeField]
    int MaxCombo = 4;

    [SerializeField]
    Feedback[] _feedbacks = {
        new Feedback(){
            Text = "Good",
            Color = Color.yellow
        },
        new Feedback(){
            Text = "Perfect !",
            Color = Color.red
        }
    };
    [SerializeField]
    [Range(0f, 2f)]
    float FeedbackDelay = 0.8f;

    private UIController _uiController;

    private BinaryFormatter _formatter;
    private ScoreData _data;

    private int _comboCounter;

    private void Start()
    {
#if UNITY_IOS && !UNITY_EDITOR
        _savePath = Application.persistentDataPath;
#endif	
        _filePath = _savePath + '/' + _saveName;
        _uiController = GetComponent<UIController>();
        _formatter = new BinaryFormatter();
        ResetUI();
    }

    void Awake()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
        } 
    }
    private void OnValidate()
    {
#if UNITY_IOS && !UNITY_EDITOR
        _savePath = Application.persistentDataPath;
#endif	
        _filePath = _savePath + '/' + _saveName;
    }

    public void SaveData()
    {
        if (!Directory.Exists(_savePath))
            Directory.CreateDirectory(_savePath);

        try
        {
            using (FileStream saveFile = File.Exists(_filePath) ? File.OpenWrite(_filePath) : File.Create(_filePath))
            {
                _formatter.Serialize(saveFile, _data);
            }
        }
        catch (Exception)
        {
            Debug.LogWarning("Fail to save at: " + _filePath);
        }
    }

    public void LoadData()
    {
        if (File.Exists(_filePath))
            using (FileStream saveFile = File.Open(_filePath, FileMode.Open))
            {
                _data = (ScoreData)(_formatter.Deserialize(saveFile) ?? _data);
            }
    }

    public void ResetUI(bool load = true)
    {
        if (load)
            LoadData();

        if (_uiController != null)
        {
            _uiController.ShowText("BestScore", _data.BestScore.ToString(), Color.grey);
            _uiController.ShowText("Score", "");
            _uiController.ShowText("Feedback", "");
        }
    }

    public void Score(bool reset = false, bool increaseCombo = false)
    {
        if (reset)
        {
            if (_data.Score > _data.BestScore)
                _data.BestScore = _data.Score;

            _data.LastScore = _data.Score;
            _data.Score = 0;
            SaveData();
            ResetUI(false);

            _uiController.ShowText("Score", "");
        }
        else
        {
            if (increaseCombo && _comboCounter < MaxCombo)
                _comboCounter++;
            else
                _comboCounter = 1;

            _data.Score += _comboCounter;

            if (_data.Score > _data.BestScore)
                _uiController.ShowText("BestScore", _data.Score.ToString(), Color.green);


            _uiController.ShowText("Score", _data.Score.ToString());

            if (increaseCombo)
                _uiController.ShowText("Feedback", _feedbacks[1].Text, _feedbacks[1].Color, FeedbackDelay, true);
            else
                _uiController.ShowText("Feedback", _feedbacks[0].Text, _feedbacks[0].Color, FeedbackDelay, true);
        }
    }
}

[Serializable]
public struct ScoreData
{
    public long BestScore;
    public long LastScore;
    [IgnoreDataMember]
    public long Score;
}

public struct Feedback
{
    public string Text;
    public Color Color;
}