﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MotionController : MonoBehaviour
{
    [Range(0.1f, 10f)]
    public float Duration = 1.5f;
    [Range(0.1f, 10f)]
    public float TimeScale = 3.5f;
    [Range(0.1f, 10f)]
    public float Speed = 2f;
    [Range(0f, 1f)]
    public float Difficulty = 0f;

    [SerializeField]
    GameObject Player = null;
    [SerializeField]
    GameObject Spawner = null;

	private ScoreController _scoreController;

    private bool _playing;

    private bool _inputRegistered;
    private bool _touching;

    private TargetSpawner _targetSpawner;
    private Target _target;

    private Vector2 _playerOriginalPosition;

    private Vector2 _targetPosition;
    private float _totalDistance;
    private float _progression;
    private float _fraction;
    private Vector2 _lastResult;
    private Vector2 _frameResult;
    private Vector2 _playerFrameResult;
    private float _targetDelta;

    private Vector2 _buffer;

    private void Start()
    {
        _targetSpawner = Spawner.GetComponent<TargetSpawner>();
		_scoreController = GetComponentInParent<ScoreController>();
        _touching = false;
        _playing = false;
    }

    private void Update()
    {
        _inputRegistered = Input.touchCount > 0 || Input.GetMouseButton(0);
        _playing = _playing || _inputRegistered;

        if (_target != null)
        {
            if (!_touching)
            {
                CheckRestart();
            }
        }

		if (!_touching && _inputRegistered && _playing)
        {
            AcquireNewTarget();
        }

        if (_target != null)
            ComputeMotion();

        _touching = _inputRegistered;
    }

    private void CheckRestart()
    { 
        bool tooLate = _target.transform.position.y < -_target.Radius;
        if (!tooLate)
        {
            if (_inputRegistered)
                _playing = _target.Collied(transform.position, Difficulty);
            else
                _playing = true;
        }
        else
        {
            _playing = !tooLate;
        }

        if (!_playing)
        {
            _scoreController.Score(true);
            _target = null;
            _lastResult = Vector2.zero;
            _targetSpawner.DestroyAll();
            _targetSpawner.Spawn(_targetSpawner.MaxTargetCount, true);
            _buffer.Set(0, 0);
            Player.transform.position = _buffer;
        }
    }

    private void AcquireNewTarget()
    {
        if (_target != null)
        {
            _target.Passed = true;
            _targetSpawner.Despawn(_target.transform.gameObject, Duration);
            _targetSpawner.Spawn(offsetFromLast: true);
        }

        _target = _targetSpawner.GetNextTarget().GetComponent<Target>();
        _targetPosition = _target.transform.position;
        _playerOriginalPosition = Player.transform.position;
        _progression = 0f;
        _fraction = 0f;
        _frameResult = default(Vector2);
        _totalDistance = (_playerOriginalPosition - _targetPosition).magnitude;
    }

    private void ComputeMotion()
    {
        _progression += Time.deltaTime * Speed;
        _fraction = _progression  / _totalDistance;

        _lastResult = _frameResult;
        _frameResult = Vector2.LerpUnclamped(_playerOriginalPosition, _targetPosition, _fraction);
        _playerFrameResult = Vector2.LerpUnclamped(_playerOriginalPosition, _target.transform.position, _fraction);
        _targetDelta = _lastResult.y - _frameResult.y;
        _buffer.Set(0, _targetDelta);
        _targetSpawner.MoveTargets(_buffer);
        _buffer.Set(_playerFrameResult.x, 0);
        Player.transform.position = _buffer;
        
    }

    private void OnValidate()
    {
        Time.timeScale = TimeScale;
    }
}
