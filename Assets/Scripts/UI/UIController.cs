﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public void ShowText(string textTag, string message, Color? color = null, float? duration = null, bool playFx = false)
    {
        GameObject textObject = GameObject.FindGameObjectWithTag(textTag);
        if (textObject == null)
        {
            Debug.LogWarning("No gameobject with tag: " + textTag);
            return;
        }

        StopAllCoroutines();

        Text text = textObject.GetComponent<Text>();
		text.text = message;
        text.enabled = true;

        if (color.HasValue)
            text.color = color.Value;

        if (duration.HasValue)
            StartCoroutine(DelayAction(() => text.enabled = false , duration.Value));

        if (playFx)
        {
            IFx fx = textObject.GetComponent<IFx>();
            if (fx != null)
                fx.Play();
        }
    }

    private IEnumerator DelayAction(Action action, float delay)
    {
        yield return new WaitForSeconds(delay);
        action();
    }
}
