﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 10f)]
    float Factor = 1f;

    private void Update()
    {
        transform.Rotate(Vector3.right, Time.deltaTime * Factor * 100f);
    }
}
