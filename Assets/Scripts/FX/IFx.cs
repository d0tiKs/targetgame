﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IFx
{
    event EventHandler FxDone;
    void Play(Action callback = null);
}
