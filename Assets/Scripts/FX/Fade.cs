﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Fade : MonoBehaviour, IFx
{
    public event EventHandler FxDone;

    internal virtual void OnFxDone(EventArgs e)
    {
        EventHandler handler = FxDone;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    [SerializeField]
    [Range(0.01f, 1f)]
    float Duration = 0.1f;

    [SerializeField]
    [Range(0, 1)]
    float MinTransparency = 0;

    private Renderer[] _renderers;
    private Color[] _colors;
    private float[] _progressions;

    private void Start()
    {
        _renderers = GetComponentsInChildren<Renderer>();
        _colors = new Color[_renderers.Length];
        _progressions = new float[_renderers.Length];

        for (int i = 0; i < _renderers.Length; i++)
        {
            _colors[i] = _renderers[i].material.color;
            _colors[i].a = 0f;
        }
    }

    public void Play(Action callback = null)
    {
        StartCoroutine(Fading(callback));
    }

    private IEnumerator Fading(Action callback)
    {
        float time = 0;
        while (time < Duration)
        {
            for (int i = 0; i < _renderers.Length; i++)
            {
                var renderer = _renderers[i];       
                var progression = (_progressions[i] += Time.deltaTime) / Duration;
                renderer.material.color = Color.Lerp(renderer.material.color, _colors[i], progression);
            }
            time += Time.deltaTime;
            yield return null;
        }

        if (callback != null)
            callback.Invoke();
    }


}
