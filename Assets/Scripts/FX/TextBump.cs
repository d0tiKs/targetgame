﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextBump : MonoBehaviour , IFx
{
	[SerializeField]
	float InDuration = 0.1f;
	[SerializeField]
	float OutDuration = 0.2f;

	[SerializeField]
	[Range(1f, 2f)]
	float BumpFactor = 1.2f;

	private Text _text;
    private bool _playing;
    private Action _callback;

    private bool _inDone = false;
    private bool _outDone = false;
    private float _clock;
    private float _progression;
    private Vector3 _originalSize;

    public event EventHandler FxDone;
	internal void OnFxDone()
    {
        EventHandler handler = FxDone;
        if (handler != null)
        {
            handler(this, new EventArgs());
        }
    }
	private void Start() {
        _playing = false;
        _text = GetComponent<Text>();
        _originalSize = _text.transform.localScale;
    }

    public void Play(Action callback = null)
    {
        _playing = true;
        _callback = callback;
        _clock = 0f;
        _progression = 0f;
    }

    private void Update()
    {
        if (!_playing)
            return;


        Debug.Log(_text.transform.localScale);

        if (!_inDone)
        {
            if (_progression > 1)
            {
                _clock = 0;
                _progression = 0;
                _inDone = true;
            }
            else
                _text.transform.localScale = Vector3.Lerp(_originalSize, _originalSize * BumpFactor, _progression);
            _progression = _clock / OutDuration;
        }
        else
        {
            if (!_outDone)
            {
                if (_progression > 1)
                    _outDone = true;
                else
                    _text.transform.localScale = Vector3.Lerp(transform.localScale, _originalSize, _progression);
                _progression = _clock / OutDuration;
            }
            else
            {
                Done();
            }
        }
        _clock += Time.deltaTime;
    }

    private void Done()
    {
        if (_callback != null)
            _callback.Invoke();
        _text.transform.localScale = _originalSize;
        _playing = false;
        _inDone = false;
        _outDone = false;
        _clock = 0f;
        _progression = 0f;
        OnFxDone();
    }
}
