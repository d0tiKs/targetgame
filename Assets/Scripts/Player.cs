﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 3f)]
    float CameraOffset = 2f;

    private MotionController _motionController;

    private void Start()
    {
        _motionController = GetComponentInParent<MotionController>();
    }

    private void Update()
    {
        UpdateCameraPosition();
    }

    private void LateUpdate()
    {
    }

    private void OnValidate()
    {
        UpdateCameraPosition();
    }

    private void UpdateCameraPosition()
    {
        Camera.main.transform.position = new Vector3(0, transform.position.y + CameraOffset, -10);
    }
}