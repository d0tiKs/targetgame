﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public bool Passed = false;
    public float Radius { get { return transform.lossyScale.y; } }

    [SerializeField]
    [Range(0.1f, 5f)]
    float BaseRadius = 0.1f;
    [SerializeField]
    [Range(0.1f, 3f)]
    float PerfectRadius = 1f;

    private IFx _destroyEffect;
	private ScoreController _scoreController;
    private TargetSpawner _targetSpawner;

    [SerializeField]
    Color[] FeedBackColors =
    {
        Color.yellow,
        Color.red
    };

    private string _feedBack;
    private Color _feedBackColor;

    private void Start()
    {
        transform.localScale = new Vector3(Radius, Radius, Radius);
        transform.GetChild(0).localScale = new Vector3(PerfectRadius, PerfectRadius, PerfectRadius);

		_scoreController = GetComponentInParent<ScoreController>();
        _targetSpawner = GetComponentInParent<TargetSpawner>();
        _destroyEffect = GetComponent<IFx>();
    }

    private void LateUpdate()
    {
    }

    public bool Collied(Vector2 playerPosition, float offset = 0f)
    {
		bool result = false;
		bool perfect = false;
        //float distance = Mathf.Abs((playerPosition - (Vector2)transform.position).magnitude);
        float position = Mathf.Abs(transform.position.y);

		result =  position <= Radius- offset;
		perfect = position <= PerfectRadius - offset;


		if (_scoreController != null) 
		{
			_scoreController.Score(!result, perfect);
		}


        return result;
    }

    public void DestroyTarget(float duration)
    {
        if (_destroyEffect != null)
            _destroyEffect.Play(() => Destroy(gameObject));
        else
            Destroy(gameObject, duration);
    }

    private void OnDestroy()
    {
        _targetSpawner.DestroyTarget(gameObject);
    }
		

    private void OnValidate()
    {
        transform.localScale = new Vector3(BaseRadius, BaseRadius, BaseRadius);
        transform.GetChild(0).localScale = new Vector3(PerfectRadius, PerfectRadius, PerfectRadius);
    }
}
