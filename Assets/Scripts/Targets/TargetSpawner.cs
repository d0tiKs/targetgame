﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject TargetModel;

    [SerializeField]
    List<GameObject> Targets  = new List<GameObject>();

    [SerializeField]
    [Range(1, 10)]
    public uint MaxTargetCount = 5;
    [SerializeField]
    [Range(0, 80)]
    int MinLatitude = 30;
    [SerializeField]
    [Range(0, 80)]
    int MaxLatitude = 80;
    [SerializeField]
    [Range(-30, 30)]
    int MinLongitude = -25;
    [SerializeField]
    [Range(-30, 30)]
    int MaxLongitude = 25;
    [SerializeField]
    [Range(0, 100)]
    int MovingTargetRatio = 10;

    private System.Random _rng = new System.Random();
    private GameObject _last;

    private void Start()
    {
        Spawn(MaxTargetCount);
    }

    public void Spawn(uint count, bool reset = false)
    {
        if (reset)
            _last = null;
        for (uint i = 0; i < count; i++)
        {
            GameObject target = Spawn(offsetFromLast : true);
        }
    }

    public GameObject Spawn(float postionOffsetX = 0f, float positionOffsetY = 0f, bool offsetFromLast = false)
    {
        if (Targets.Count < MaxTargetCount)
        {
            if (offsetFromLast && _last != null)
                positionOffsetY = _last.transform.position.y;
            float longitude = 0, latitude = 0;
            try
            {
                longitude = (_rng.Next(MinLongitude, MaxLongitude) * .1f) - +postionOffsetX;
                latitude = (_rng.Next(MinLatitude, MaxLatitude) * .1f) + positionOffsetY + 0.001f;
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }

            GameObject newTarget = Instantiate(TargetModel, transform);
            if (_rng.Next(0, 100) <= MovingTargetRatio)
            {
                newTarget.AddComponent<MovingTarget>();
            }

            newTarget.transform.position = new Vector3(longitude, latitude);

            Targets.Add(newTarget);
            _last = newTarget;

            return newTarget;
        }
        return null;
    }

    public void MoveTargets(Vector3 delta)
    {
        foreach (var target in Targets)
            target.transform.position += delta;
    }

    public void Despawn(GameObject target, float duration = 0f)
    {
        target.GetComponent<Target>().DestroyTarget(duration);
    }

    public GameObject GetNextTarget()
    {
        for (int i = 0; i < Targets.Count; i++)
        {
            if (!Targets[i].GetComponent<Target>().Passed)
                return Targets[i];
        }

        return null;
    }

    public void DestroyTarget(GameObject target)
    {
        Targets.Remove(target);
    }

    public void DestroyAll()
    {
        foreach (var target in Targets)
        {
            Destroy(target);
        }

        Targets.Clear();
    }
}
