﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTarget : MonoBehaviour
{
    [SerializeField]
    [Range(0.1f, 5f)]
    float Speed = 2f;

    [SerializeField]
    [Range(0f, 2f)]
    float EdgeLimit = .15f;

    private Vector3 _originalPosition;
    private int _direction = 1;
    private float _progression = 0f;
    private float _fraction;
    private Vector2 _buffer;

    private float halfScreenWitdh;

    private void Start()
    {
        halfScreenWitdh = Camera.main.orthographicSize / 2;
        _originalPosition = transform.position;

        var rng = new System.Random(this.GetHashCode());
        if (rng.Next(0, 1) == 0)
            _direction = -1;

        Speed += (rng.Next(-10, 10) * .1f);
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        _progression += Time.deltaTime * Speed;
        _fraction = Mathf.Abs(_progression / halfScreenWitdh);

        if (_fraction < 1)
        {
            float result = Mathf.LerpUnclamped(_originalPosition.x, _direction * (halfScreenWitdh - EdgeLimit), _fraction);
            _buffer.Set(result, transform.position.y);
            transform.position = _buffer;
        }
        else
        {
            _originalPosition = transform.position;
            _progression = 0;
            _direction *= -1;
        }
    }
}
